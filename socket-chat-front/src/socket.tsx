import io from 'socket.io-client';

import {
    loginUser,
    newMessage
} from './Store/actions';

import {
    SEND_MESSAGE,
    SEND_USER
} from './Store/const';

export const socket = io('http://localhost:8000');


export const socketMiddleware = (store: any) => {
    socket.on('connect', () => {
        console.log('socket connected');
    });

    socket.on('new message', (data: any) => {
        store.dispatch(newMessage(data.message.author, data.message.message))
    });

    socket.on('login', (data: any) => {
        store.dispatch(loginUser(data.username))
        store.dispatch(newMessage('system', `Welcome to chat. There are ${data.numUsers-1} people`, true))
    });

    socket.on('user joined', (data: any) => {
        const state = store.getState();
        if (data.username !== state.user.userName) {
            store.dispatch(newMessage('system', `${data.username} joined to the chat. There are ${data.numUsers-1} people`, true))
        }
    });

    socket.on('user left', (data: any) => {
        store.dispatch(newMessage('system', `${data.username} left the chat. There are ${data.numUsers-1} people`, true))
    });

    return (next: any) => (action: any) => {
        switch (action.type) {

            case SEND_MESSAGE:
                socket.emit('new message', action.payload);
                break;

            case SEND_USER:
                socket.emit('add user', action.payload.username);
                break;

            default:
                break;
        }

        return next(action)
    };
};

