import { Message } from '../types';

import {
    SEND_MESSAGE,
    SEND_USER,
    LOGIN_USER,
    NEW_MESSAGE
} from './const';


interface UserData {
    username: string
}

interface AppChatMessage extends  Message {
    isSystem: boolean
}

interface IAppAction {
    type: string
}

export interface IAppMessageAction extends IAppAction{
    payload: Message | AppChatMessage
}

export interface IAppUserAction extends IAppAction{
    payload: UserData
}

export const sendMessage = (message: Message):IAppMessageAction => {
    return {
        type: SEND_MESSAGE,
        payload: message
    };
};


export const newUser = (username: string):IAppUserAction => {
    return {
        type: SEND_USER,
        payload: {
            username
        }
    };
};


export const loginUser = (username: string):IAppUserAction => {
    return {
        type: LOGIN_USER,
        payload: {
            username
        }
    };
};


export const newMessage = (author: string, message: string, isSystem: boolean = false):IAppMessageAction => {
    return {
        type: NEW_MESSAGE,
        payload: {
            author,
            message,
            isSystem
        }
    };
};
