export const SEND_MESSAGE: string = 'SEND_MESSAGE';
export const SEND_USER: string = 'SEND_USER';
export const LOGIN_USER: string = 'LOGIN_USER';
export const NEW_MESSAGE: string = 'NEW_MESSAGE';
