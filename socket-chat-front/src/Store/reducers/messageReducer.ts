import { MessagesState } from '../../types';
import { IAppMessageAction } from '../actions';


const initialMessagesState: MessagesState = {
    messages: []
};


export const messageReducer = (state = initialMessagesState, action: IAppMessageAction): MessagesState => {
    switch(action.type) {
        case "NEW_MESSAGE":
            const newMessage = action.payload;
            return {
                ...state,
                messages: [...state.messages, newMessage]
            };
        default:
            return state
    }
};


