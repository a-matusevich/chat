import { UserState } from '../../types';
import { IAppUserAction } from '../actions';

const initialUserState: UserState = {
    userName: ''
};



export const userReducer = (state = initialUserState, action: IAppUserAction): UserState => {
    switch(action.type) {
        case "LOGIN_USER":
            return {
                ...state,
                userName: action.payload.username
            };
        default:
            return state
    }
};

