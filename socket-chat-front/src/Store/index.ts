import { createStore, combineReducers, applyMiddleware } from 'redux';
import { userReducer } from './reducers/userReducer';
import { messageReducer } from './reducers/messageReducer';
import { socketMiddleware } from '../socket'

const rootReducer = combineReducers({
    user: userReducer,
    messages: messageReducer
});


export const store = createStore(
    rootReducer,
    applyMiddleware(socketMiddleware)
)

