import { createSelector } from 'reselect';
import { prop } from 'ramda';

import { IStore } from '../types'
const getUserData = (state: IStore) => state.user;
//@ts-ignore
export const getUsername = createSelector(getUserData, prop('userName'))

const getMessagesData = (state: IStore) => state.messages;
export const getMessagesList = createSelector(getMessagesData, prop('messages'))
