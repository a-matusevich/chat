import React from 'react';
import Chat from './Components/Chat/Chat';

const App: React.FC = () => {
  return (
    <div className="App">
      <Chat />
    </div>
  );
};

export default App;
