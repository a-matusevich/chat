
export interface UserState {
    userName?: string,
}
export interface MessagesState {
    messages: Array<Message>
}

export interface IStore {
    user: UserState,
    messages: MessagesState
}

export type Message = {
    author: string,
    message: string
}
