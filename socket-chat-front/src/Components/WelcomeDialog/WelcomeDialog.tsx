import React from "react";
import { connect } from 'react-redux';
import { Dispatch } from "redux";

import { withStyles } from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Button from "@material-ui/core/Button";

import { newUser } from '../../Store/actions';

interface IWelcomeDialogProps {
    classes: any,
    sendUser?: (username: string) => void
}

interface IWelcomeDialogState {
    username: string
}

interface IDialogMessageDispatchProps {
    sendUser: (username: string) => void;
}

const styles = {
    welcomeDialog: {
        border: 'solid',
        width: '400px'
    },
    gridBox: {
        height: '100%'
    },
    form: {
        width: '100%',
    },
    button: {
        marginRight: 'auto',
        marginLeft: 'auto'
    }
};

class WelcomeDialog extends React.Component<IWelcomeDialogProps, IWelcomeDialogState> {

    constructor(props: IWelcomeDialogProps) {
        super(props);

        this.state = {
            username: ''
        };
    }

    handleChange = (event: any):void => {
        this.setState({username: event.target.value});
    };

    handleSubmit = (event: any): void => {
        event.preventDefault();
        if (this.state.username.length === 0) {
            return
        }
        //@ts-ignore
        this.props.sendUser(this.state.username);
        this.setState((): IWelcomeDialogState => {
             return { username: "" };
        });
    };

    render() {
        const classes = this.props.classes;
        const { username } = this.state;


        return (
            <Grid
                container={true}
                justify='center'
                alignItems='center'
                className={classes.gridBox}
            >
                <Box
                    className={classes.welcomeDialog}
                    display='flex'
                    flexDirection='column'
                    alignItems='center'
                    marginRight='auto'
                    marginLeft='auto'
                    padding='20px'
                >
                    <Box textAlign='center'>
                        Welcome to socket chat. What is your nickname?
                    </Box>
                    <form onSubmit={this.handleSubmit} className={classes.form}>
                        <Box
                            display='flex'
                            flexDirection='column'
                            alignSelf='stretch'
                            marginTop='20px'
                            marginBottom='20px'

                        >
                            <Input
                                onChange={this.handleChange}
                                autoFocus={true}
                                id='username'
                                type='text'
                                className={classes.input}
                                placeholder='Type here...'
                                autoComplete='Off'
                                value={username}
                            />
                        </Box>
                        <Grid
                            container={true}
                            justify='center'
                            alignItems='center'
                        >
                            <Button
                                variant="contained"
                                color="default"
                                className={classes.button}
                                type="submit"
                                href=''
                            >
                                Send
                            </Button>
                        </Grid>
                    </form>



                </Box>
            </Grid>
        )
    }
}


const mapDispatchToProps = (dispatch: Dispatch): IDialogMessageDispatchProps => ({
    sendUser: (username: string) => dispatch(newUser(username))
});

export default connect(null, mapDispatchToProps)(withStyles(styles)(WelcomeDialog));
