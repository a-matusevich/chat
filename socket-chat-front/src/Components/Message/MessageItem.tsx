import React from "react";
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";

interface IMessageItemProps {
    message: string,
    author: string,
    username: string
    isSystem: boolean,
    classes: any
}


const styles: any = {
    messageItem: {
        border: `solid 1px`,
        borderRadius: `3px`,
        minWidth: `20%`,
        margin: `10px 10px 10px 5px`,
        padding: '10px',
        maxWidth: '80%'
    }
};

class MessageItem extends React.Component<IMessageItemProps, {}> {

    render() {

        const classes = this.props.classes;

        if (this.props.isSystem) {
            return (
                <Box
                    className={classes.messageItem}
                    display='flex'
                    alignSelf='center'
                >
                    {this.props.message}
                </Box>
            )
        }

        const isMessageMine: boolean = this.props.username === this.props.author

        return (
            <Box
                className={classes.messageItem}
                display='flex'
                alignSelf={isMessageMine ? "flex-end" : "flex-start"}
            >
                <Box
                    display='flex'
                    flexDirection='column'
                >
                    <Box fontStyle="italic" fontWeight='bold' fontSize='18px'>
                        {this.props.author}
                    </Box>
                    <Box fontSize='20px'>
                        {this.props.message}
                    </Box>
                </Box>
            </Box>
        )
    }
}

const mapStateToProps = (state: any) => ({
    username: state.user.userName
});

export default connect(mapStateToProps)(withStyles(styles)(MessageItem));
