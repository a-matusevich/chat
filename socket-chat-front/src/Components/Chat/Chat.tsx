import React from "react";
import { connect } from 'react-redux';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core/styles';

import MessageList from '../MessagesList/MessageList';
import MessageSender from '../MessageSender/MessageSender';
import WelcomeDialog from '../WelcomeDialog/WelcomeDialog';

import { getUsername } from '../../Store/selectors';

import { IStore } from '../../types';

interface IChatProps {
    classes: any,
    userName?: string
}

const styles: any = {
    mainContainer: {
        height: `100vh`,
        padding: 0,
        minWidth: `260px`
    }
};

class Chat extends React.Component<IChatProps, {}> {

    getMainBox = (userName: string | undefined): React.ReactElement => {
        if (userName) {
            return (
                <Box
                    display='flex'
                    flexDirection='column'
                    alignItems='stretch'
                    height='100%'
                >
                    <MessageList/>
                    <MessageSender/>
                </Box>
            )
        } else {
           return (
                <WelcomeDialog />
            )
        }
    }

    render() {
        const { userName } = this.props;
        const mainBox = this.getMainBox(userName);

        return (
            <React.Fragment>
                <CssBaseline />
                <Container
                    className={this.props.classes.mainContainer}
                    maxWidth="md"
                >
                    {mainBox}
                </Container>
            </React.Fragment>
        )
    }
}


const mapStateToProps = (state: IStore) => {
    return({
        userName: getUsername(state)
    });
};

export default connect(mapStateToProps)(withStyles(styles)(Chat));
