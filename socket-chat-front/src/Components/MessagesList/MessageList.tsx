import React from "react";
import { connect } from 'react-redux';
import uuid from 'uuid/v1'

import { withStyles } from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";

import MessageItem from '../Message/MessageItem'

import { getMessagesList } from '../../Store/selectors';
import { IStore, Message } from '../../types';

const styles = {
    messageList: {
        border: '3px solid',
        marginBottom: '10px'
    }
};


interface IMessageListProps {
    classes: any,
    messagesList: Array<Message>
}

class MessageList extends React.Component<IMessageListProps, {}> {

    getMessagesListElement = (messagesList: Array<Message>): Array<React.ReactElement> | null => messagesList
            ? messagesList.map((message:any) => <MessageItem key={uuid()} author={message.author} message={message.message} isSystem={message.isSystem}/>)
            : null;


    render() {
        const { messagesList, classes } =  this.props;
        const messagesElement = this.getMessagesListElement(messagesList);

        return (
            <Box
                component='div'
                overflow='hidden scroll'
                className={classes.messageList}
                flexGrow={95}
                display='flex'
                flexDirection='column'
                alignItems='center'
            >
                {messagesElement}
            </Box>
        )
    }
}


const mapStateToProps = (state: IStore) => ({
    messagesList: getMessagesList(state)
});

export default connect(mapStateToProps)(withStyles(styles)(MessageList));
