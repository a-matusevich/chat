import React from "react";
import { connect } from 'react-redux';
import { Dispatch } from 'redux'

import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';

import { IStore, Message } from '../../types';

import { sendMessage } from '../../Store/actions';


export interface MessageSenderProps {
    classes: any,
    sendMessage?: any,
    userName?: string
}

export interface MessageSenderState {
    message: string,
    author: string
}

export interface IMessageSenderDispatchProps {
    sendMessage: (message: Message) => void;
}


const styles = {
    messageSender: {
        marginBottom: '10px'
    },
    input: {
        width: `100%`,
        marginRight: '10px'
    },
    button: {
        minWidth: `100px`
    },
    form: {
        width: '100%',
        display: 'flex',
        justifyContent: 'stretch'
    }
};

class MessageSender extends React.Component<MessageSenderProps, MessageSenderState> {
    constructor(props: MessageSenderProps) {
        super(props);

        this.state = {
            message: '',
            author: props.userName || ''
        };
    }

    handleChange = (event: any) => {
        this.setState({message: event.target.value});
    };

    handleSubmit = (event: any) => {
        event.preventDefault();
        if (this.state.message.length === 0) {
            return
        }
        const message: Message = {
            author: this.state.author,
            message: this.state.message
        };
        this.props.sendMessage(message);
        this.setState((state: MessageSenderState): MessageSenderState => {
            return {
                ...state,
                message: ""
            };
        });
    };

    render() {
        const classes = this.props.classes;
        const { message } = this.state;
        return (
            <Box
                className={classes.messageSender}
                display='flex'
                justifyContent='stretch'
                padding='0 10px'
            >
                <form onSubmit={this.handleSubmit} className={classes.form} >
                    <Input
                        onChange={this.handleChange}
                        autoFocus={true}
                        id='message'
                        type='text'
                        className={classes.input}
                        placeholder='Type here...'
                        autoComplete='Off'
                        value={message}
                    />
                    <Button variant="contained" className={classes.button} type="submit" href=''>
                        Send
                    </Button>
                </form>
            </Box>
        )
    }
}


const mapStateToProps = (state: IStore) => ({
    userName: state.user.userName
});


const mapDispatchToProps = (dispatch: Dispatch): IMessageSenderDispatchProps => ({
    sendMessage: (message: Message) => dispatch(sendMessage(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MessageSender));
